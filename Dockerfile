FROM node:12.18.0-alpine3.12

ARG appPath=/srv/app/todoql
RUN mkdir -p ${appPath}
WORKDIR ${appPath}

COPY package.json yarn.lock  ./

RUN yarn

COPY . ${appPath}

EXPOSE 8080

CMD ["yarn", "dev"]
