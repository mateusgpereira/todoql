<img src="files/logo_readme.jpeg" width="250px" height="200px" alt="react graphql nodejs Logo">

# TodoQL
An graphql api with node JS

### Clone

- Clone this repo to your local machine using

```shell
git clone https://gitlab.com/mateusgpereira/todoql.git
```

### Run on Dev
- First copy the .env.example on the project root folder, to just .env in the same directory
```shell
cd todoql
cp .env.example .env
```

- Then start the mongodb database and the application process on a docker container with the docker-compose command
```shell
docker-compose up -d
```
This will build the image, with the dependecies and to run in a local environment


The Api is not finished, for now is possible to List, Create, Update and Delete Todos
And Create and Login with Users


### FrontEnd
There is a front end app for this api on [this link!](https://gitlab.com/mateusgpereira/frontend)
