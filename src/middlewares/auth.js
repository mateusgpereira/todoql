const jwt = require('jsonwebtoken')
const User = require('../models/user')
const { secret } = require('../config/auth')

const auth = async (req, res, next) => {
  try {
    const [, token] = req.header('Authorization').split(' ')
    const decoded = jwt.verify(token, secret)
    const { password, ...user } = await User.findById(decoded.userId)

    if (!user) {
      throw new Error()
    }

    req.user = user
    req.isAuth = true
    next()
  } catch (e) {
    req.isAuth = false
    next()
  }
}

module.exports = auth
