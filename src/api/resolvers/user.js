const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const User = require('../../models/user')
const { expiresIn, secret } = require('../../config/auth')

module.exports = {
  createUser: async (args) => {
    const hash = await bcrypt.hash(args.userInput.password, 8)
    const user = new User({
      email: args.userInput.email,
      password: hash
    })

    const createdUser = await user.save()
    return { ...createdUser._doc, password: null }
  },
  login: async ({ email, password }) => {
    const user = await User.findOne({ email })

    if (!user) {
      throw Error('User does not exists!')
    }

    const isRight = await bcrypt.compare(password, user.password)
    if (!isRight) {
      throw Error('Invalid credentials!')
    }

    const token = jwt.sign({ userId: user.id, email }, secret, { expiresIn })

    return { userId: user.id, token, tokenExpiration: parseInt(expiresIn, 10) }
  }
}
