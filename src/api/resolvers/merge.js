const Todo = require('../../models/todo')
const User = require('../../models/user')

const fetchTodos = async (todoIds) => {
  const todos = await Todo.find({ _id: { $in: todoIds } })
  return todos.map((todo) => {
    return {
      ...todo._doc,
      owner: fetchUser.bind(this, todo.owner)
    }
  })
}

const fetchUser = async (userId) => {
  const user = await User.findById(userId)
  return {
    ...user._doc,
    todos: fetchTodos.bind(this, user.todos)
  }
}

module.exports = { fetchTodos, fetchUser }
