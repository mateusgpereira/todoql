const Todo = require('../../models/todo')
const User = require('../../models/user')
const { fetchUser } = require('./merge')

module.exports = {
  todos: async (args, req) => {
    if (!req.isAuth) {
      throw Error('Please provide authentication!')
    }

    try {
      const todos = await Todo.find({ owner: req.user._doc._id })
      return todos.map((todo) => {
        return {
          ...todo._doc,
          owner: fetchUser.bind(this, todo.owner)
        }
      })
    } catch (e) {
      throw Error("Couldn't retrieve todos")
    }
  },
  createTodo: async (args, req) => {
    if (!req.isAuth) {
      throw Error('Please provide authentication!')
    }
    const todo = new Todo({
      description: args.todoInput.description,
      completed: args.todoInput.completed,
      owner: req.user._doc._id
    })

    const user = await User.findById(req.user._doc._id)
    if (!user) {
      throw Error('User Not Found')
    }

    const createdTodo = await todo.save()
    user.todos.push(todo)
    await user.save()
    return { ...createdTodo._doc }
  },
  updateTodo: async (args, req) => {
    if (!req.isAuth) {
      throw Error('Please provide authentication!')
    }

    try {
      const todo = await Todo.findById(args.todoInput._id)
      if (!todo) {
        throw Error('Todo not found')
      }

      const allowedUpdates = ['description', 'completed']

      allowedUpdates.forEach((update) => {
        if (args.todoInput[update] != null) {
          todo[update] = args.todoInput[update]
        }
      })

      await todo.save()
      return todo
    } catch (e) {
      console.log(e)
      throw e
    }
  },
  deleteTodo: async (args, req) => {
    if (!req.isAuth) {
      throw Error('Please provide authentication!')
    }

    try {
      const todo = await Todo.findOneAndDelete({
        _id: args.id,
        owner: req.user._doc._id
      })

      if (!todo) {
        throw Error('Todo not found')
      }

      return todo
    } catch (e) {
      console.log(e)
      throw e
    }
  }
}
