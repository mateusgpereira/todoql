const userResolver = require('./user')
const todosResolver = require('./todos')

module.exports = { ...userResolver, ...todosResolver }
