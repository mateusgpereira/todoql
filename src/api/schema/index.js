const { buildSchema } = require('graphql')

module.exports = buildSchema(`
  type Todo {
    _id: ID!
    description: String!
    completed: Boolean!
    owner: User!
  }

  type User {
    _id: ID!
    email: String!
    password: String
    todos: [Todo!]
  }

  type AuthData {
    userId: ID!
    token: String!
    tokenExpiration: Int!
  }

  input TodoInput {
    description: String!
    completed: Boolean
  }

  input TodoUpdateInput {
    _id: ID!
    description: String
    completed: Boolean
  }

  input UserInput {
    email: String!
    password: String!
  }

  type RootQuery {
    todos: [Todo!]!
    login(email: String!, password: String!): AuthData!
  }

  type RootMutation {
    createTodo(todoInput: TodoInput): Todo
    updateTodo(todoInput: TodoUpdateInput): Todo
    deleteTodo(id: ID!): Todo
    createUser(userInput: UserInput): User
  }

  schema {
    query: RootQuery
    mutation: RootMutation
  }
`)
