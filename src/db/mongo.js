const mongoose = require('mongoose')

mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useFindAndModify: true
  })
  .then()
  .catch((err) => {
    console.log(err)
  })
