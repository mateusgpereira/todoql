require('dotenv/config')
const express = require('express')
const graphqlHttp = require('express-graphql')
const graphqlSchema = require('./api/schema/index')
const resolvers = require('./api/resolvers/index')
const auth = require('./middlewares/auth')

require('./db/mongo')

const app = express()

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200)
  }
  next()
})

app.use(express.json())

app.use(auth)

app.use(
  '/graphql',
  graphqlHttp({
    schema: graphqlSchema,
    rootValue: resolvers,
    graphiql: true
  })
)

module.exports = app
